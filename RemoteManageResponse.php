<?php
/**
 * Extend remoteControl new add_response_code function.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2022 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 2.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class RemoteManageResponse extends PluginBase {
    protected $storage = 'DbStorage';
    static protected $description = 'Extend remoteControl new add_response_code function.';
    static protected $name = 'RemoteManageResponse';

    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => 'Allow usage of add_response_code function',
            'default'=> false
        ),
        'replaceDefaultLink' => array(
            'type' => 'boolean',
            'label' => 'Replace default remotecontrol link, try to conect with this plugin before leave default',
            'help' => '',
            'default' => 0,
        )
    );

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    public function init()
    {
        if(Yii::app()->getConfig("RPCInterface") != 'json') {
            return;
        }
        $this->subscribe('newDirectRequest');
        $this->subscribe('newUnsecureRequest','newDirectRequest');

        /* replace default connexion */
        if ($this->get('replaceDefaultLink')) {
            $this->subscribe('beforeControllerAction');
        }
    }

    /**
     * Update the information content to show the good link
     * @params getValues
     */
    public function getPluginSettings($getValues=true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $this->settings['information']['content'] = "";
        /* test if plugins/unsecure is in noCsrfValidationRoutes : in internal for compatible LimeSurvey version */
        if(in_array('plugins/unsecure',App()->request->noCsrfValidationRoutes)) {
            $url = App()->getController()->createAbsoluteUrl('plugins/unsecure', array('plugin' => $this->getName(), 'function' => 'action'));
        } else {
            $this->settings['information']['content'].="<p class='alert alert-warning'>You need to add 'plugins/direct' to noCsrfValidationRoutes in your config file</p>";
            $url = App()->getController()->createAbsoluteUrl('plugins/direct', array('plugin' => $this->getName(), 'function' => 'action'));
        }
        if(Yii::app()->getConfig("RPCInterface") == 'json') {
            $this->settings['information']['content'].="<p class='alert alert-info'>".sprintf(gT("The remote url was <code>%s</code>",'unescaped'),$url)."</p>";
            if(Yii::app()->getConfig("rpc_publish_api") == true) {
                if(floatval(Yii::app()->getConfig("versionnumber"))>=2.5)
                {
                    $url= App()->getController()->createAbsoluteUrl('admin/pluginhelper', array('plugin' => $this->getName(), 'sa'=>'sidebody','method'=>'actionIndex','surveyId'=>0));
                }
                $this->settings['information']['content'].="<p class='alert alert-warning'>".sprintf(gT("The API was published on <a href='%s'>%s</a>",'unescaped'),$url,$url)."</p>";
            }
            /* functioon added */
            $this->settings['information']['content'] .= "<div class='well'>";
            $this->settings['information']['content'] .= "<p>" . gT("Allow usage of add_response_code function: same than add_response with 3 parameters") ."</p>";
            $this->settings['information']['content'] .= "<ul>";
            $this->settings['information']['content'] .= "<li>" . gT("existingId : can be replace, replaceanswers, renumber or ignore (default to ignore);") ."</li>";
            $this->settings['information']['content'] .= "<li>" . gT("emCode : allow usage of emCode directly, else use sgqa only (default to true);") . "</li>";
            $this->settings['information']['content'] .= "<li>" . gT("vvReplacement : do the extra VV replacement (“{quote}”, “{tab}”, “{cr}”, “{newline}”, “{lbrace}”), {question_not_shown} is always replaced and set to null (default to true).") . "</li>";
            $this->settings['information']['content'] .= "</ul>";
        } else {
            $this->settings['information']['content'] = "<p class='alert alert-danger'>".gT("JSON RPC is not active.")."</p>";
        }
        return parent::getPluginSettings($getValues);
    }

    /**
     * The access is done here for remoteControl access with plugin
     * @see remotecontrol::run()
     */
    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->getEvent();
        if ($oEvent->get('target') != $this->getName()) {
            return;
        }
        $action = $oEvent->get('function');

        $oAdminController = new \AdminController('admin/remotecontrol');
        Yii::import('application.helpers.remotecontrol.*');
        Yii::setPathOfAlias('RemoteManageResponse', dirname(__FILE__));
        Yii::import("RemoteManageResponse.RemoteControlHandler");
        $oHandler=new \RemoteControlHandler($oAdminController);
        $RPCType=Yii::app()->getConfig("RPCInterface");
        if($RPCType!='json')
        {
            header("Content-type: application/json");
            echo json_encode(array(
                'status'=>'error',
                'message'=>'Only for json RPCInterface',
            ));
            Yii::app()->end();
        }
        if (Yii::app()->request->getIsPostRequest())
        {
            Yii::app()->loadLibrary('LSjsonRPCServer');
            if (!isset($_SERVER['CONTENT_TYPE']))
            {
                $serverContentType = explode(';', $_SERVER['HTTP_CONTENT_TYPE']);
                $_SERVER['CONTENT_TYPE'] = reset($serverContentType);
            }
            LSjsonRPCServer::handle($oHandler);
        }
        elseif(Yii::app()->getConfig("rpc_publish_api") == true) // Show like near Core LS do it
        {
            $reflector = new ReflectionObject($oHandler);
            foreach ($reflector->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                /* @var $method ReflectionMethod */
                if (substr($method->getName(),0,1) !== '_') {
                    $list[$method->getName()] = array(
                        'description' => str_replace(array("\r", "\r\n", "\n"), "<br/>", $method->getDocComment()),
                        'parameters'  => $method->getParameters()
                    );
                }
            }
            ksort($list);
            $aData['method'] = $RPCType;
            $aData['list'] = $list;

            $version=floatval(Yii::app()->getConfig("versionnumber"));
            if($version<2.5)
            {
                $content=$oAdminController->renderPartial('application.views.admin.remotecontrol.index_view',$aData,true);
                $oEvent->setContent($this, $content);
            }
            else // Show something for 2.5, but 2.5 have a better system
            {
                return $oAdminController->render('application.views.admin.remotecontrol.index_view',$aData);
            }
        }
    }

    /**
     * Show remote control function list in 2.50
     * Used by PluginHelper->getContent
     * @see remotecontrol::run()
     */
    public function actionIndex()
    {
        if(Yii::app()->getConfig("RPCInterface")=='json' && Yii::app()->getConfig("rpc_publish_api"))
        {
            $oAdminController = new \AdminController('admin/remotecontrol');
            Yii::import('application.helpers.remotecontrol.*');
            Yii::setPathOfAlias('RemoteManageResponse', dirname(__FILE__));
            Yii::import("RemoteManageResponse.RemoteControlHandler");
            $oHandler=new \RemoteControlHandler($oAdminController);
            $reflector = new ReflectionObject($oHandler);
            foreach ($reflector->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                /* @var $method ReflectionMethod */
                if (substr($method->getName(),0,1) !== '_') {
                    $list[$method->getName()] = array(
                        'description' => $method->getDocComment(),
                        'parameters'  => $method->getParameters()
                    );
                }
            }
            ksort($list);
            $aData['method'] = 'json';
            $aData['list'] = $list;
            return Yii::app()->controller->renderPartial('application.views.admin.remotecontrol.index_view', $aData, true);
        }
    }

    /**
     * Check if current function call are
     */
    public function beforeControllerAction()
    {
        $event = $this->getEvent();
        if($event->get('controller') != 'admin' && $event->get('action') != 'remotecontrol') {
            return;
        }
        if (!App()->request->getIsPostRequest()) {
            return;
        }
        $request = @json_decode(file_get_contents('php://input'), true);
        if (!isset($request['method']) || !isset($request['params'])) {
            return;
        }
        $method = $request['method'];
        if ($method != 'add_response_code' && $method != 'recompute_response') {
            return;
        }
        $this->setRemoteHandler();
        $event->set('run', false);
    }

    /**
     * set the handler to own
     * @return void
     */
    private function setRemoteHandler()
    {
        $oAdminController = new \AdminController('admin/remotecontrol');
        Yii::import('application.helpers.remotecontrol.*');
        Yii::setPathOfAlias('RemoteManageResponse', dirname(__FILE__));
        Yii::import("RemoteManageResponse.RemoteControlHandler");

        /* LSYii_Controller.php : Sepond time if needed */
        Yii::import("application.helpers.globalsettings");

        /* in Survey_Common_Action */
        Yii::import('application.helpers.viewHelper');
        Yii::import('application.controllers.admin.NotificationController');
        Yii::import('application.libraries.Date_Time_Converter');

        $oHandler = new \RemoteControlHandler($oAdminController);
        App()->loadLibrary('LSjsonRPCServer');
        if (!isset($_SERVER['CONTENT_TYPE'])) {
            $serverContentType = explode(';', $_SERVER['HTTP_CONTENT_TYPE']);
            $_SERVER['CONTENT_TYPE'] = reset($serverContentType);
        }
        LSjsonRPCServer::handle($oHandler);
    }
}
