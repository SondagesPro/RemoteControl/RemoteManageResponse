# RemoteManageResponse

2 new functions to manage response via remote control :
- one to easily create or update reponse with question code with option to replace response
- the other to update Equation and other response value (set to null if needed)

This plugin is currently tested on 5.4.0.

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06)
- Clone in plugins/RemoteManageResponse directory

### Via ZIP dowload
- Get the file and uncompress it
- Move the file included to plugins/RemoteManageResponse directory

## Usage

This plugin extend [LimeSurvey RemoteControl 2 API](https://manual.limesurvey.org/RemoteControl_2_API). New function are added to LimeSurvey API with the new url created by the plugin. 

You can call the new function with the link shown in plugin settings. If you publish API in LimeSurvey core config : another url is show where you can see all available function (the new one and the other from LimeSurvey core).

You can activate the plugin option globally : it check if function is one `add_response_code` or `recompute_response`

### Functions detail

#### add_response_code

Add a response to the survey responses collection and returns the id of the inserted survey response

`add_response_code($sSessionKey, $iSurveyID, $aResponseData[, $existingId='ignore'][, $emCode=true])`

- string $sSessionKey Auth credentials 
- int $iSurveyID ID of the Survey to insert responses
- struct $aResponseData The actual response data in an array with key for code or columns name.
- string $existingId if is is set :
    - replace : replace all response by current data
    - replaceanswers : replace existing column, keep other columns (update an answer)
    - renumber : create a new response with another number
    - ignore : don't add response, return an error witgh status set to `Ignore exsiting id`
- boolean $emCode allowed to use EM code, else use only columns name

Return the response id in case of success, array with status in case of error.

The function did not include the feature to move files from upload directory to survey directory.

#### recompute_response

Recompute equation and relevance after updating a reponse or a survey.

`recompute_response($sSessionKey, $surveyid, $srid)`

- string $sSessionKey Auth credentials
- integer $surveyid ID of the Survey to insert responses
- integer $srid Tthe response to be updated

Return the number of response updated (1 currently), array with status in case of error.

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/extendremotecontrol/>
- Copyright © 2015-2022 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>

## Support
- Issues <https://gitlab.com/SondagesPro/RemoteControl/RemoteManageResponse/issues>
- Professional support <https://www.sondages.pro/contact.html>
