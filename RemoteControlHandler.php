<?php

/**
 * Handler for extendRemoteControl Plugin for LimeSurvey : add yours functions here
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2022 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 2.2.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class RemoteControlHandler extends remotecontrol_handle
{
    /**
    * Add a response to the survey responses collection.
    * Returns the id of the inserted survey response
    *
    * @param string $sSessionKey Auth credentials
    * @param int $iSurveyID ID of the Survey to insert responses
    * @param struct $aResponseData The actual response
    * @param string $existingId if is is set : replace|replaceanswers|renumber|ignore
    * @param boolean $emCode allowed to use EM code
    * @param boolean $vvReplacement do the extra VV replacement ("{quote}", "{tab}", "{cr}", "{newline}", "{lbrace}") ({question_not_shown} is alwways replaced)
    * @return int|array The response ID if success
    */
    public function add_response_code($sSessionKey, $iSurveyID, $aResponseData, $existingId = 'ignore', $emCode = true, $vvReplacement = true)
    {
        if (!$this->_checkSessionKey($sSessionKey)) {
            return array('status' => 'Invalid session key');
        }
        $oSurvey = Survey::model()->findByPk($iSurveyID);
        if (is_null($oSurvey)) {
            return array('status' => 'Error: Invalid survey ID');
        }
        if (!Yii::app()->db->schema->getTable('{{survey_' . $iSurveyID . '}}')) {
            return array('status' => 'No survey response table');
        }
        if ($existingId == 'replace' || $existingId == 'replaceanswers') {
            if (!Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'update')) {
                return array('status' => 'No permission');
            }
        }
        if (!Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'create') && !Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'import')) {
            return array('status' => 'No permission');
        }
        if (in_array($existingId, array('replace','replaceanswers')) && !Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'update')) {
            return array('status' => 'No update permission');
        }
        /* OK, we can import */
        $aRealFieldNames = Yii::app()->db->getSchema()->getTable(SurveyDynamic::model($iSurveyID)->tableName())->getColumnNames();
        LimeExpressionManager::SetDirtyFlag($iSurveyID); // Be sure survey EM code are up to date : think session is clean by default but :/
        $aLemFieldNames = LimeExpressionManager::getLEMqcode2sgqa($iSurveyID);

        $aResponse = array();
        foreach ($aResponseData as $code => $value) {
            if (in_array($code, $aRealFieldNames)) {
                $aResponse[$code] = $value;
            } elseif ($emCode && isset($aLemFieldNames[$code])) {
                $aResponse[$aLemFieldNames[$code]] = $value;
            }
        }
        if (empty($aResponse)) {
            return array('status' => 'No answers could be mapped.');
        }
        // Comment because if it's not set : leave it not set, no ?
        /**
        if( !isset($aResponseData['submitdate']) ) {
            $aResponseData['submitdate'] = date("Y-m-d H:i:s");
        } elseif( empty($aResponseData['submitdate']) ) { // Mimic add_response : strange ?
            unset($aResponseData['submitdate']);
        }
        */
        if ($oSurvey->datestamp == 'Y') {
            if (!isset($aResponse['datestamp'])) {
                $aResponse['datestamp'] = date("Y-m-d H:i:s");
            } elseif (empty($aResponse['datestamp'])) {
                unset($aResponse['datestamp']);
            }
            if (!isset($aResponse['startdate'])) {
                $aResponse['startdate'] = date("Y-m-d H:i:s");
            } elseif (empty($aResponse['startdate'])) {
                unset($aResponse['startdate']);
            }
        }
        if (isset($aResponse['id']) && (string)$aResponse['id'] !== (string)(int)$aResponse['id']) {
            unset($aResponse['id']);
        }
        if (isset($aResponse['id'])) {
            $oResponse = Response::model($iSurveyID)->find('id=:id', array(":id" => $aResponse['id']));
            if ($oResponse) {
                switch ($existingId) {
                    case 'replace':
                        $oResponse->delete(true);
                        unset($oResponse);
                        break;
                    case 'replaceanswers':
                        unset($aResponse['id']);
                        break;
                    case 'renumber':
                        unset($aResponse['id']);
                        unset($oResponse);
                        break;
                    case 'ignore':
                    default:
                        break;
                }
            }
        }
        if (empty($oResponse)) {
            $oResponse = Response::create($iSurveyID);
        }
        foreach ($aResponse as $attribute => $value) {
            if ($value == '{question_not_shown}') {
                $oResponse->$attribute = new CDbExpression('NULL');
            } else {
                $valueFixed = $value;
                if ($vvReplacement) {
                    $valueFixed = str_replace(array("{quote}", "{tab}", "{cr}", "{newline}", "{lbrace}"), array("\"", "\t", "\r", "\n", "{"), $value);
                }
                $oResponse->setAttribute($attribute, $valueFixed);
            }
        }
        $oTransaction = Yii::app()->db->beginTransaction();
        try {
            if (isset($aResponse['id'])) {
                switchMSSQLIdentityInsert('survey_' . $iSurveyID, true);
            }
            if ($oResponse->save()) {
                $oTransaction->commit();
                return $oResponse->id;
            }
            $oTransaction->rollBack();
            if (isset($aResponse['id'])) {
                switchMSSQLIdentityInsert('survey_' . $iSurveyID, false);
            }
            $errors = $oResponse->getErrors();
            return array('status' => 'Unable to add response : {$errors[0]}' ); // Return 1st error
        } catch (Exception $oException) {
            return array('status' => 'Unable to add response : ' . $oException->getMessage());
        }
        /*Can not come here */
        return array('status' => 'Unable to add response'); // Add validation error ?
    }

    /**
    * Do the expression and validation again
    *
    * @param string $sSessionKey Auth credentials
    * @param int $iSurveyID ID of the Survey to insert responses
    * @param integer $srid Tthe response to be updated
    * @return int|array The response ID if success
    */
    public function recompute_response($sSessionKey, $surveyid, $srid)
    {
        if (!$this->_checkSessionKey($sSessionKey)) {
            return array('status' => 'Invalid session key');
        }
        $oSurvey = Survey::model()->findByPk($surveyid);
        if (is_null($oSurvey)) {
            return array('status' => 'Error: Invalid survey ID');
        }
        if (!Yii::app()->db->schema->getTable('{{survey_' . $surveyid . '}}')) {
            return array('status' => 'No survey response table');
        }
        if (!Permission::model()->hasSurveyPermission($surveyid, 'responses', 'update')) {
            return array('status' => 'No permission');
        }
        if (in_array($existingId, array('replace','replaceanswers')) && !Permission::model()->hasSurveyPermission($surveyid, 'responses', 'update')) {
            return array('status' => 'No update permission');
        }
        $oResponse = Response::model($surveyid)->findByPk($srid);
        if (empty($oResponse)) {
            return array('status' => 'Response do no exist');
        }
        $keepTrack = array(
            'submitdate' => $oResponse->submitdate,
            'lastpage' => $oResponse->lastpage,
        );
        if ($oSurvey->isDateStamp) {
            $keepTrack['datestamp'] = $oResponse->datestamp;
        }
        /* Start survey */
        killSurveySession($surveyid);
        \LimeExpressionManager::SetSurveyId($surveyid);
        \LimeExpressionManager::SetEMLanguage($oResponse->startlanguage);
        $_SESSION['survey_' . $surveyid] = array(
            's_lang' => $oResponse->startlanguage,
            'srid' => $oResponse->id,
        );
        buildsurveysession($surveyid);
        /* Load answers */
        $surveyId = $surveyid;
        $insertarray = $_SESSION['survey_' . $surveyId]['insertarray'];
        $fieldmap = $_SESSION['survey_' . $surveyId]['fieldmap'];
        foreach($oResponse->getAttributes() as $column => $value) {
            switch ($column) {
                case "datestamp":
                case "startdate":
                case "startlanguage":
                case "token": // ?
                    $_SESSION['survey_' . $surveyId][$column] = $value;
                    break;
                default:
                    if (in_array($column, $insertarray) && isset($fieldmap[$column])) {
                        if(in_array($fieldmap[$column]['type'], array('N','K','D'))) {
                            if($value === null) {
                                $value = '';
                            }
                        }
                        $_SESSION['survey_'.$surveyId][$column] = $value;
                    }
            }
        }
        /* Submit forced */
        $_SESSION['survey_'.$surveyid]['step'] = 0;
        $_SESSION['survey_'.$surveyid]['srid'] = $oResponse->id;
        /* Start the survey as group survey */
        \LimeExpressionManager::StartSurvey($surveyid, 'group', $this->_getSurveyOption($oSurvey, $oResponse), false);
        \LimeExpressionManager::JumpTo(0,false,false,true); // no preview, no post and force
        /* Set submitdate to null to allow submit in all condtion */
        Response::model($surveyid)->updateByPk($oResponse->id, array('submitdate' => null));
        /* SAVEDID is update only after save value in DB in expression manager */
        \LimeExpressionManager::setValueToKnowVar('SAVEDID', $oResponse->id);
        /* Go to count of all groups +1 */
        if (App()->getConfig('versionnumber') > 3) {
            $numberOfGroups = \QuestionGroup::model()->count(
                "sid = :sid",
                array(
                    ":sid" => $surveyid,
                )
            );
        } else {
            $numberOfGroups = \QuestionGroup::model()->count(
                "sid = :sid AND language = :language",
                array(
                    ":sid" => $surveyid,
                    ":language" => $oResponse->startlanguage
                )
            );
        }
        if($numberOfGroups > 0) {
            /* Here the save action is done : after submitdate is set */
            $aMoveResult = \LimeExpressionManager::JumpTo($numberOfGroups+1, false, false, true); // Force if equation in last group
        }
        /* Reset */
        Response::model($surveyid)->updateByPk(
            $oResponses->id,
            $keepTrack
        );
        return 1;
    }

    /**
     * Create a ExpressionManager survey option
     * @param \Survey $oSurvey
     * @param \Response $oResponse
     * @return void
     */
    private function _getSurveyOption($oSurvey, $oResponse)
    {
        $token = null;
        if (isset($oResponse->token)) {
            $token = $oResponse->token;
        }
        return array(
            'active' => $oSurvey->active == 'Y',
            'allowsave' => true,
            'anonymized' => $oSurvey->anonymized != 'N',
            'assessments' => false,
            'datestamp' => $oSurvey->datestamp == 'Y',
            'deletenonvalues' => Yii::app()->getConfig('deletenonvalues'),
            'hyperlinkSyntaxHighlighting' => false,
            'ipaddr' => $oSurvey->ipaddr == 'Y',
            'radix' => ".",
            'refurl' => null,
            'savetimings' => false,
            'surveyls_dateformat' =>  1,
            'startlanguage' => $oResponse->startlanguage,
            'target' => Yii::app()->getConfig('uploaddir') . DIRECTORY_SEPARATOR . 'surveys' . DIRECTORY_SEPARATOR . $surveyid . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR,
            'tempdir' => Yii::app()->getConfig('tempdir') . DIRECTORY_SEPARATOR,
            'timeadjust' => Yii::app()->getConfig("timeadjust"),
            'token' => $token,
        );
    }

}
